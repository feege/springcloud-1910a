package com.gaofei.user.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gaofei.user.domain.User;

/**
 * @author : gaofee
 * @date : 16:25 2022/6/5
 * @码云地址 : https://feege.gitee.io
 */
public interface UserMapper extends BaseMapper<User> {
}
