package com.gaofei;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author : gaofee
 * @date : 15:53 2022/5/23
 * @码云地址 : https://feege.gitee.io
 */
@SpringBootApplication
public class MongoApp {
    public static void main(String[] args) {
        SpringApplication.run(MongoApp.class, args);
    }
}
