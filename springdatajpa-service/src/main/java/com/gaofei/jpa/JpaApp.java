package com.gaofei.jpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 总结出来结论了
 * @author : gaofee
 * @date : 11:01 2022/5/24
 * @码云地址 : https://feege.gitee.io
 */
@SpringBootApplication
public class JpaApp {
    public static void main(String[] args) {
        SpringApplication.run(JpaApp.class, args);
    }
}
