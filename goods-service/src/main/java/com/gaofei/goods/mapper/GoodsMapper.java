package com.gaofei.goods.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gaofei.goods.domain.User;

/**
 * @author : gaofee
 * @date : 15:05 2022/6/6
 * @码云地址 : https://feege.gitee.io
 */
public interface GoodsMapper extends BaseMapper<User> {
}
