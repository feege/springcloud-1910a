package com.gaofei.auth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gaofei.auth.domain.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author : gaofee
 * @date : 11:53 2022/5/26
 * @码云地址 : https://feege.gitee.io
 */
public interface UserMapper extends BaseMapper<User> {


}
